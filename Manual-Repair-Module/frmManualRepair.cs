﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ontology_Module;
using OntologyClasses.BaseClasses;
using ElasticSearchLogging_Module;
using OntologyAppDBConnector;
using ElasticSearchNestConnector;
using ImportExport_Module;

namespace Manual_Repair_Module
{
    public partial class frmManualRepair : Form
    {

        private clsLocalConfig objLocalConfig;
        private clsDataWork_Repair objDataWork_Repair;
        private clsTransaction_Objects objTransaction_Objects;
        private clsRelationConfig objRelationConfig;
        private clsTransaction objTransaction;

        private OntologyModDBConnector objDBLevel;

        public frmManualRepair()
        {
            InitializeComponent();

            objLocalConfig = new clsLocalConfig(new Globals());
            objTransaction = new clsTransaction(objLocalConfig.Globals);
            objRelationConfig = new clsRelationConfig(objLocalConfig.Globals);
            objDataWork_Repair = new clsDataWork_Repair(objLocalConfig);
            objDataWork_Repair.GetData_Log();
            //objTransaction_Objects = new clsTransaction_Objects(objLocalConfig.Globals);
            //Initialize();
            //Repair_MathematischeKomponenten();
            //Repair_MP3File();
            //Repair_MultipleRelations();
            //RemoveMultipleDataTypes();
            //Move_Versions();
            //MoveLogEntries();
            //DeleteAllReportFields();
            //ExportOntologyItems();
            //ImportOntologyItems();
            //GetDoubleObjects();
            //ReIndexDoubleObjects();
            //CopyIndex();

            //MoveLiteraturquellen();
            //Consolidate_Bankkontos();
            //TestMultipleClassRelations();
            //TestBankTransaktionen();
            //DeleteRelations("0dd35db6a50a4d47b568a466337ec747", "6f497caad60b4847b346b50f4f5a0f54", "e07469d9766c443e85266d9c684f944f", 6);
            DeleteBookmarks();
        }


        private void DeleteBookmarks()
        {
            objDBLevel = new OntologyModDBConnector(objLocalConfig.Globals);
            var searchBookmarks = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Parent_Object = "e51633cae76f4a3bb4eb95aace386755",
                    ID_RelationType = "1f063a79ddde44c595ba50391fe6f0e3"
                }
            };

            var result = objDBLevel.GetDataObjectRel(searchBookmarks);


        }

        private void DeleteRelations(string idObject, string idOther, string idRelationType, long? orderId = null)
        {
            var query = "ID_Object:" + idObject + " AND ID_Other:" + idOther + " AND ID_RelationType:" + idRelationType;

            if (orderId != null)
            {
                query += " AND OrderID:" + orderId;
            }
            var dbSelector = new clsAppDBLevel(objLocalConfig.Globals.Server, objLocalConfig.Globals.Port, objLocalConfig.Globals.Index, objLocalConfig.Globals.SearchRange, objLocalConfig.Globals.Session);
            var documents = dbSelector.GetData_Documents(objLocalConfig.Globals.Index, "ObjectRel", false, 0, query);

            dbSelector.Del_Documents("ObjectRel", documents.Select(doc => doc.Id).ToList());


        }

        private void TestBankTransaktionen()
        {
            objDBLevel = new OntologyModDBConnector(objLocalConfig.Globals);
            var searchTransaktionen = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Parent_Object = "6401e369444549a398221029a4c1b8b8",
                    ID_Parent_Other = "d4ef7be6afaf42e2859661d70b886c23",
                    ID_RelationType = "06b93890d92247edb70e5e9cbd03df52"
                }
            };

            var result = objDBLevel.GetDataObjectRel(searchTransaktionen);

            var multiple = (from trans in objDBLevel.ObjectRels
                            group trans by new { ID_Object = trans.ID_Object, ID_Other = trans.ID_Other, ID_RelationType = trans.ID_RelationType } into transes
                            select new { Key = transes.Key, Count = transes.Count() })
                            .Where(trans => trans.Count > 1).ToList();

        }

        private void Consolidate_Bankkontos()
        {
            objDBLevel = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel1 = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel2 = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel3 = new OntologyModDBConnector(objLocalConfig.Globals);

            var searchBankkonten = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = "d4ef7be6afaf42e2859661d70b886c23"
                }
            };

            var result = objDBLevel1.GetDataObjects(searchBankkonten);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var bankKontoWithCount = from bankkonto in objDBLevel1.Objects1
                                         group bankkonto by bankkonto.Name into bankkontos
                                         select new { Name_Bankkonto = bankkontos.Key, Count_Bankkonto = bankkontos.Count() };

                var searchRelLeftRight = objDBLevel1.Objects1.Select(bankKonto => new clsObjectRel { ID_Object = bankKonto.GUID }).ToList();
                var searchRelRightLeft = objDBLevel1.Objects1.Select(bankKonto => new clsObjectRel { ID_Other = bankKonto.GUID }).ToList();

                result = objDBLevel2.GetDataObjectRel(searchRelLeftRight);
                if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    result = objDBLevel3.GetDataObjectRel(searchRelRightLeft);
                    if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        bankKontoWithCount.Where(bkonto => bkonto.Count_Bankkonto > 1).ToList().ForEach(bkonto =>
                        {
                            var allKontos = objDBLevel1.Objects1.Where(bankKonto => bankKonto.Name == bkonto.Name_Bankkonto).ToList();

                            var firstKonto = allKontos.First();
                            var delKontos = allKontos.Where(konto => konto.GUID != firstKonto.GUID).ToList();

                            var relationsOtherKontosLeftRight = objDBLevel2.ObjectRels.Where(other => other.ID_Object != firstKonto.GUID && other.Name_Object == firstKonto.Name).ToList();
                            var relationsOtherKontosRightLeft = objDBLevel3.ObjectRels.Where(other => other.ID_Other != firstKonto.GUID && other.Name_Other == firstKonto.Name).ToList();

                            var relationsOtherKontosLeftRight_Save = relationsOtherKontosLeftRight.Select(other => 
                                objRelationConfig.Rel_ObjectRelation(firstKonto, new clsOntologyItem
                                    {
                                        GUID = other.ID_Other,
                                        Name = other.Name_Other,
                                        GUID_Parent = other.ID_Parent_Other,
                                        Type = other.Ontology
                                    },
                                    new clsOntologyItem
                                    {
                                        GUID = other.ID_RelationType,
                                        Name = other.Name_RelationType
                                    },
                                    orderId:other.OrderID.Value)
                            ).ToList();

                            var relationsOtherKontosRightLeft_Save = relationsOtherKontosRightLeft.Select(other =>
                                objRelationConfig.Rel_ObjectRelation(new clsOntologyItem
                                {
                                    GUID = other.ID_Object,
                                    Name = other.Name_Object,
                                    GUID_Parent = other.ID_Parent_Object,
                                    Type = other.Ontology
                                }, firstKonto,
                                    new clsOntologyItem
                                    {
                                        GUID = other.ID_RelationType,
                                        Name = other.Name_RelationType
                                    },
                                    orderId: other.OrderID.Value)
                            ).ToList();

                            if (relationsOtherKontosLeftRight_Save.Any())
                            {
                                result = objDBLevel.SaveObjRel(relationsOtherKontosLeftRight_Save);
                            }
                            

                            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID || result.GUID == objLocalConfig.Globals.LState_Nothing.GUID)
                            {
                                if (relationsOtherKontosRightLeft_Save.Any())
                                {
                                    result = objDBLevel.SaveObjRel(relationsOtherKontosRightLeft_Save);
                                }
                                

                                if (result.GUID == objLocalConfig.Globals.LState_Success.GUID || result.GUID == objLocalConfig.Globals.LState_Nothing.GUID)
                                {
                                    if (relationsOtherKontosLeftRight.Any())
                                    {
                                        result = objDBLevel.DelObjectRels(relationsOtherKontosLeftRight);
                                    }
                                    
                                    if (result.GUID == objLocalConfig.Globals.LState_Success.GUID || result.GUID == objLocalConfig.Globals.LState_Nothing.GUID)
                                    {
                                        if (relationsOtherKontosRightLeft.Any())
                                        {
                                            result = objDBLevel.DelObjectRels(relationsOtherKontosRightLeft);
                                        }
                                        
                                        if (result.GUID == objLocalConfig.Globals.LState_Success.GUID || result.GUID == objLocalConfig.Globals.LState_Nothing.GUID)
                                        {
                                            objDBLevel.DelObjects(delKontos);
                                        }
                                    }
                                }
                            }
                            
                        });
                    }
                }
            }
        }


        private void TestMultipleClassRelations()
        {
            var dbSelector = new clsAppDBLevel(objLocalConfig.Globals.Server, objLocalConfig.Globals.Port, objLocalConfig.Globals.Index, objLocalConfig.Globals.SearchRange, objLocalConfig.Globals.Session);



            var pos = 0;
            var count = 1;

            var clsRels = new List<clsClassRel>();
            while (count > 0)
            {
                var documents = dbSelector.GetData_Documents(objLocalConfig.Globals.Index, "ClassRel", true, lastPos: pos);

                clsRels.AddRange(documents.Select(doc => new clsClassRel
                {
                    ID_Class_Left = doc.Dict.ContainsKey("ID_Class_Left") ? doc.Dict["ID_Class_Left"] != null ? doc.Dict["ID_Class_Left"].ToString() : null : null,
                    ID_Class_Right = doc.Dict.ContainsKey("ID_Class_Right") ? doc.Dict["ID_Class_Right"] != null ? doc.Dict["ID_Class_Right"].ToString() : null : null,
                    ID_RelationType = doc.Dict.ContainsKey("ID_RelationType") ? doc.Dict["ID_RelationType"] != null ? doc.Dict["ID_RelationType"].ToString() : null : null

                }));

                count = documents.Count;
                pos = dbSelector.LastPos;
            }

            var multiple = (from objRel in clsRels
                            group objRel by new { ID_Left = objRel.ID_Class_Left, ID_Right = objRel.ID_Class_Right, ID_RelationType = objRel.ID_RelationType } into objRels
                            select new { Key = objRels.Key, Count = objRels.Count() }).Where(rels => rels.Count > 1).ToList();

            dataGridView1.DataSource = multiple;
        }

        private void TestMultipleObjectRelations()
        {
            var dbSelector = new clsAppDBLevel(objLocalConfig.Globals.Server, objLocalConfig.Globals.Port, objLocalConfig.Globals.Index, objLocalConfig.Globals.SearchRange, objLocalConfig.Globals.Session);

            

            var pos = 0;
            var count = 1;

            var objectRels = new List<clsObjectRel>();
            while(count>0)
            {
                var documents = dbSelector.GetData_Documents(objLocalConfig.Globals.Index, "ObjectRel", true, lastPos:pos);

                objectRels.AddRange(documents.Select(doc => new clsObjectRel
                {
                    ID_Object = doc.Dict.ContainsKey("ID_Object") ? doc.Dict["ID_Object"] != null ? doc.Dict["ID_Object"].ToString(): null : null,
                    ID_Parent_Object = doc.Dict.ContainsKey("ID_Parent_Object") ? doc.Dict["ID_Parent_Object"] != null ? doc.Dict["ID_Parent_Object"].ToString() : null : null,
                    ID_Other = doc.Dict.ContainsKey("ID_Other") ? doc.Dict["ID_Other"] != null ? doc.Dict["ID_Other"].ToString() : null : null,
                    ID_Parent_Other = doc.Dict.ContainsKey("ID_Parent_Other") ? doc.Dict["ID_Parent_Other"] != null ? doc.Dict["ID_Parent_Other"].ToString() : null : null,
                    ID_RelationType = doc.Dict.ContainsKey("ID_RelationType") ? doc.Dict["ID_RelationType"] != null ? doc.Dict["ID_RelationType"].ToString() : null : null,
                    Ontology = doc.Dict.ContainsKey("Ontology") ? doc.Dict["Ontology"] != null ? doc.Dict["Ontology"].ToString() : null : null,
                    OrderID = doc.Dict.ContainsKey("OrderID") ? doc.Dict["OrderID"] != null ? (long)doc.Dict["OrderID"] : 0 : 0,

                }));

                count = documents.Count;
                pos = dbSelector.LastPos;
            }

            var multiple = (from objRel in objectRels
                            group objRel by new { ID_Left = objRel.ID_Object, ID_Right = objRel.ID_Other, ID_RelationType = objRel.ID_RelationType } into objRels
                            select new { Key = objRels.Key, Count = objRels.Count() }).Where(rels => rels.Count > 1).ToList();

            dataGridView1.DataSource = multiple;
        }

        private void CopyIndex()
        {

            var dbSelector = new clsDBSelector(objLocalConfig.Globals.Server, objLocalConfig.Globals.Port, objLocalConfig.Globals.Index, objLocalConfig.Globals.Index_Rep,objLocalConfig.Globals.SearchRange, objLocalConfig.Globals.Session);

            var copyIndex = new clsCopyIndex(dbSelector);
            copyIndex.CopyIndex(objLocalConfig.Globals.Index + "_new", CopyItem.AttributeTypes);
            copyIndex.CopyIndex(objLocalConfig.Globals.Index + "_new", CopyItem.ClassAttributes);
            copyIndex.CopyIndex(objLocalConfig.Globals.Index + "_new", CopyItem.Classes);
            copyIndex.CopyIndex(objLocalConfig.Globals.Index + "_new", CopyItem.ClassRelations);
            copyIndex.CopyIndex(objLocalConfig.Globals.Index + "_new", CopyItem.DataTypes);
            copyIndex.CopyIndex(objLocalConfig.Globals.Index + "_new", CopyItem.ObjectAttributes);
            copyIndex.CopyIndex(objLocalConfig.Globals.Index + "_new", CopyItem.ObjectRelations);
            copyIndex.CopyIndex(objLocalConfig.Globals.Index + "_new", CopyItem.Objects);
            copyIndex.CopyIndex(objLocalConfig.Globals.Index + "_new", CopyItem.RelationTypes);





        }

        private void MoveLiteraturquellen()
        {
            objDBLevel = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel1 = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel2 = new OntologyModDBConnector(objLocalConfig.Globals);

            var searchLiteraturQuellen = new List<clsObjectRel> { new clsObjectRel
                {
                    ID_Parent_Object = "08e1c993269346a39f27a4540090aaaa",
                    ID_Parent_Other = "769df8b4ae0f4343a1109cd7eadcd3b3"
                }
            };

            var result = objDBLevel.GetDataObjectRel(searchLiteraturQuellen);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var searchUrls = new List<clsObjectRel> {  new clsObjectRel
                    {
                        ID_Parent_Object = "769df8b4ae0f4343a1109cd7eadcd3b3",
                        ID_Parent_Other = "094d728d6efc463c85c72dcfed903c78"
                    }
                };

                result = objDBLevel1.GetDataObjectRel(searchUrls);


            }

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var searchNewBlog = objDBLevel1.ObjectRels.Select(objRel => new clsObjectRel
                {
                    ID_Other = objRel.ID_Other,
                    ID_Parent_Object = "d8782656a1e64af089b968ae9c0b81fa"
                }).ToList();

                result = objDBLevel2.GetDataObjectRel(searchNewBlog);


            }

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var blogLiteraturQuellen = (from newBlog in objDBLevel2.ObjectRels
                                  join oldBlog in objDBLevel1.ObjectRels on newBlog.ID_Other equals oldBlog.ID_Other
                                  join literaturQuelle in objDBLevel.ObjectRels on oldBlog.ID_Object equals literaturQuelle.ID_Other
                                  select new clsObjectRel
                                  {
                                      ID_Object = literaturQuelle.ID_Object,
                                      ID_Parent_Object = literaturQuelle.ID_Parent_Object,
                                      ID_Other = newBlog.ID_Object,
                                      ID_Parent_Other = newBlog.ID_Parent_Object,
                                      ID_RelationType = literaturQuelle.ID_RelationType,
                                      Ontology = objLocalConfig.Globals.Type_Object,
                                      OrderID = literaturQuelle.OrderID
                                  }).ToList();

                objDBLevel.SaveObjRel(blogLiteraturQuellen);
            }
        }

        private void MoveSeitenBlogs()
        {
            objDBLevel = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel1 = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel2 = new OntologyModDBConnector(objLocalConfig.Globals);

            var searchSeiten = new List<clsObjectAtt> { new clsObjectAtt
                {
                    ID_AttributeType = "608c25a65a5242cb9c270acbf4cfe648",
                    ID_Class = "769df8b4ae0f4343a1109cd7eadcd3b3"
                }
            };

            var result = objDBLevel.GetDataObjectAtt(searchSeiten);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var searchUrls = new List<clsObjectRel> {  new clsObjectRel
                    {
                        ID_Parent_Object = "769df8b4ae0f4343a1109cd7eadcd3b3",
                        ID_Parent_Other = "094d728d6efc463c85c72dcfed903c78"
                    }
                };

                result = objDBLevel1.GetDataObjectRel(searchUrls);

               
            }

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var searchNewBlog = objDBLevel1.ObjectRels.Select(objRel => new clsObjectRel
                {
                    ID_Other = objRel.ID_Other,
                    ID_Parent_Object = "d8782656a1e64af089b968ae9c0b81fa"
                }).ToList();

                result = objDBLevel2.GetDataObjectRel(searchNewBlog);


            }

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var blogSeiten = (from newBlog in objDBLevel2.ObjectRels
                                  join oldBlog in objDBLevel1.ObjectRels on newBlog.ID_Other equals oldBlog.ID_Other
                                  join seitenItem in objDBLevel.ObjAtts on oldBlog.ID_Object equals seitenItem.ID_Object
                                  select new clsObjectAtt
                                  {
                                      ID_Attribute = seitenItem.ID_Attribute,
                                      ID_AttributeType = seitenItem.ID_AttributeType,
                                      ID_Object = newBlog.ID_Object,
                                      ID_Class = newBlog.ID_Parent_Object,
                                      ID_DataType = seitenItem.ID_DataType,
                                      OrderID = seitenItem.OrderID,
                                      Val_Lng = seitenItem.Val_Lng,
                                      Val_Name = seitenItem.Val_Name
                                  }).ToList();

                objDBLevel.SaveObjAtt(blogSeiten);
            }

                
        }

        private void ReIndexDoubleObjects()
        {
            objDBLevel = new OntologyModDBConnector(objLocalConfig.Globals);
            var result = objDBLevel.GetDataObjects();
            var search = (from objGuids in objDBLevel.Objects1
                group objGuids by objGuids.GUID into GroupOfGuid
                select GroupOfGuid).Select(key => new {Guid = key.Key, Count = key.Count()}).Where(obj => obj.Count>1).ToList();

            var deleteCreate = (from obj in objDBLevel.Objects1
                join objDbl in search on obj.GUID equals objDbl.Guid
                select obj).ToList();

            var elSelector = new ElasticSearchNestConnector.clsDBSelector(objLocalConfig.Globals.Server,
                objLocalConfig.Globals.Port,
                objLocalConfig.Globals.Index,
                objLocalConfig.Globals.Index_Rep,
                objLocalConfig.Globals.SearchRange,
                objLocalConfig.Globals.Session);


            var elDeletor = new clsDBDeletor(elSelector);
            var elUpdater = new clsDBUpdater(elSelector);

            result = elDeletor.del_Objects(deleteCreate, testRelations:false);
            result = elUpdater.save_Objects(deleteCreate);
        }

        private void ExportOntologyItems()
        {
            var objExport = new clsExport(objLocalConfig.Globals);

            objExport.ExportOntologyGraph("C:\\Temp\\Ontologies");
        }

        private void ImportOntologyItems()
        {
            var objImport = new ImportWorker(objLocalConfig.Globals);

            objImport.ImportXMLFiles("C:\\Temp\\Ontologies");
        }

        private void DeleteAllReportFields()
        {
            var delRel1 = new List<clsObjectRel> { new clsObjectRel { ID_Parent_Object = "c790aa5b14a446b0bd8c4317ef5716e2" } };
            var delRel2 = new List<clsObjectRel> { new clsObjectRel { ID_Parent_Other = "c790aa5b14a446b0bd8c4317ef5716e2" } };
            var delAtts = new List<clsObjectAtt> { new clsObjectAtt { ID_Class = "c790aa5b14a446b0bd8c4317ef5716e2" } };
            var delObjects = new List<clsOntologyItem> { new clsOntologyItem { GUID_Parent = "c790aa5b14a446b0bd8c4317ef5716e2" } };

            var objDBLevel_Del = new OntologyModDBConnector(objLocalConfig.Globals);

            objDBLevel_Del.DelObjectRels(delRel1);
            objDBLevel_Del.DelObjectRels(delRel2);
            objDBLevel_Del.DelObjectAtts(delAtts);
            objDBLevel_Del.DelObjects(delObjects);
        }

        private void MoveLogEntries()
        {
            var dbLevel_LogEntry_Relations = new OntologyModDBConnector(objLocalConfig.Globals);
            var dbLevel_LogEntries = new OntologyModDBConnector(objLocalConfig.Globals);
            var dbLevel_Move = new OntologyModDBConnector(objLocalConfig.Globals);

            var searchLogentries = new List<clsObjectAtt> {
                new clsObjectAtt {
                    ID_AttributeType = "2e5fd016c5744924b724d1b30640243a"
                }};


            var result = dbLevel_LogEntry_Relations.GetDataObjectAtt(searchLogentries, doIds: false);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var searchLogentries2 = new List<clsOntologyItem> {
                    new clsOntologyItem
                    {
                        GUID_Parent = "351d45912495450182aba425f5235db9"
                }};

                result = dbLevel_LogEntries.GetDataObjects(searchLogentries2);

                if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    var toMovePre = dbLevel_LogEntry_Relations.ObjAtts.Where(loge => loge.Val_Datetime < DateTime.Now.AddDays(-90)).ToList();
                    var toMovePre2 = (from att in toMovePre
                                      join loge in dbLevel_LogEntries.Objects1 on att.ID_Object equals loge.GUID
                                      select loge).ToList();
                    var toMove = (from objLogE in toMovePre2
                                  group objLogE by new { objLogE.GUID, objLogE.Name, objLogE.GUID_Parent } into objLogEs
                                  select new clsOntologyItem
                                  {
                                      GUID = objLogEs.Key.GUID,
                                      Name = objLogEs.Key.Name,
                                      GUID_Parent = objLogEs.Key.GUID_Parent,
                                      Type = objLocalConfig.Globals.Type_Object
                                  }).ToList();

                    var objOItem_Result = objTransaction_Objects.move_Objects(toMove, "2dd8398d4cd14ef391eb30780720e25e");

                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID)
                    {
                        MessageBox.Show(this, "Fehler beim Verschieben der Logeinträge!", "Fehler!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }

                


            }
        }

        private void RemoveMultipleDataTypes()
        {
            var dBLevel_DataTypes = new OntologyModDBConnector(objLocalConfig.Globals);

            var objLogging = new clsLogging(objLocalConfig.Globals);

            objLogging.Initialize_Logging(objDataWork_Repair.Log, "multipledatatypes");

            var oItem_Result = dBLevel_DataTypes.GetDataDataTypes();

            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
            objLogging.Add_DictEntry("timestamp", DateTime.Now);
            objLogging.Add_DictEntry("step", "search datatypes");
            objLogging.Add_DictEntry("substep", "start");
            objLogging.Finish_Document();

            if (oItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                objLogging.Add_DictEntry("step", "search datatypes");
                objLogging.Add_DictEntry("result", "success");
                objLogging.Finish_Document();

                var dataTypeLists = (from objDataType in dBLevel_DataTypes.DataTypes
                                      group objDataType by objDataType.GUID into objDataTypes
                                      select new
                                      {
                                          ID_DataType = objDataTypes.Key,
                                          Count = objDataTypes.Count()
                                      }).Where(dtyp => dtyp.Count > 1)
                                      .Select(dtyp => new clsOntologyItem { GUID = dtyp.ID_DataType }).ToList();

                oItem_Result = dBLevel_DataTypes.DelDataTypes(dataTypeLists);

                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                objLogging.Add_DictEntry("step", "del datatypes");
                objLogging.Add_DictEntry("substep", "start");
                objLogging.Finish_Document();

                if (oItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                    objLogging.Add_DictEntry("timestamp", DateTime.Now);
                    objLogging.Add_DictEntry("step", "del datatypes");
                    objLogging.Add_DictEntry("result", "success");
                    objLogging.Finish_Document();
                }
                else
                {
                    objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                    objLogging.Add_DictEntry("timestamp", DateTime.Now);
                    objLogging.Add_DictEntry("step", "del datatypes");
                    objLogging.Add_DictEntry("result", "error");
                    objLogging.Finish_Document();
                }
            }
            else
            {
                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                objLogging.Add_DictEntry("step", "search datatypes");
                objLogging.Add_DictEntry("result", "error");
                objLogging.Finish_Document();
            }
        }

        private void Initialize()
        {
            objDataWork_Repair = new clsDataWork_Repair(objLocalConfig);
            var result = objDataWork_Repair.GetData_Log();

            if (result.GUID == objLocalConfig.Globals.LState_Error.GUID)
            {
                MessageBox.Show(this, "Das Log-Objekt konnte nicht ermittelt werden", "Fehler!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(-1);
            }
        }

        private void Repair_MultipleRelations()
        {
            var objLogging = new clsLogging(objLocalConfig.Globals);

            objLogging.Initialize_Logging(objDataWork_Repair.Log, "multiplerelations");

            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
            objLogging.Add_DictEntry("timestamp", DateTime.Now);
            objLogging.Add_DictEntry("step", "search multiple relations");
            objLogging.Add_DictEntry("substep", "start");
            objLogging.Finish_Document();

            var objDBLevel_RelationSearch = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel_Work = new OntologyModDBConnector(objLocalConfig.Globals);

            var result = objDBLevel_RelationSearch.GetDataObjectRel(null, doIds: true);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var multiple = (from objRel in objDBLevel_RelationSearch.ObjectRelsId
                                group objRel by new { ID_Left = objRel.ID_Object, ID_Right = objRel.ID_Other, ID_RelationType = objRel.ID_RelationType } into objRels
                                select new { Key = objRels.Key, Count = objRels.Count(), GUID = objLocalConfig.Globals.NewGUID })
                                .Where(m => m.Count > 1).ToList();

                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                objLogging.Add_DictEntry("step", "search multiple relations");
                objLogging.Add_DictEntry("count", multiple.Count());
                objLogging.Add_DictEntry("substep", "start");
                objLogging.Finish_Document();

                var relations = (from objRel in objDBLevel_RelationSearch.ObjectRelsId
                                 join objMult in multiple on new
                                 {
                                     ID_Object = objRel.ID_Object,
                                     ID_Other = objRel.ID_Other,
                                     ID_RelationType = objRel.ID_RelationType
                                 } equals
                                 new
                                 {
                                     ID_Object = objMult.Key.ID_Left,
                                     ID_Other = objMult.Key.ID_Right,
                                     ID_RelationType = objMult.Key.ID_RelationType
                                 }
                                 select new { objRel, objMult.GUID }).OrderBy(m => m.GUID).ToList();

                var relationsSave = new List<clsObjectRel>();
                var relationsDel = new List<clsObjectRel>();
                var guid = "";
                foreach (var relation in relations)
                {
                    if (guid == relation.GUID)
                    {
                        relationsDel.Add(relation.objRel);

                        objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                        objLogging.Add_DictEntry("timestamp", DateTime.Now);
                        objLogging.Add_DictEntry("step", "listadd");
                        objLogging.Add_DictEntry("ontology", relation.objRel.Ontology);
                        objLogging.Add_DictEntry("id_object", relation.objRel.ID_Object);
                        objLogging.Add_DictEntry("id_other", relation.objRel.ID_Other);
                        objLogging.Add_DictEntry("id_relationtype", relation.objRel.ID_RelationType);
                        objLogging.Add_DictEntry("mode", "delete");
                        objLogging.Finish_Document();
                    }
                    else if (guid != relation.GUID)
                    {
                        if (relation.objRel.Ontology == "Object")
                        {
                            if (!string.IsNullOrEmpty(relation.objRel.ID_Object) &&
                                !string.IsNullOrEmpty(relation.objRel.ID_Parent_Object) &&
                                !string.IsNullOrEmpty(relation.objRel.ID_Other) &&
                                !string.IsNullOrEmpty(relation.objRel.ID_Parent_Other) &&
                                !string.IsNullOrEmpty(relation.objRel.ID_RelationType) &&
                                relation.objRel.OrderID != null)
                            {
                                relationsSave.Add(relation.objRel);

                                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                objLogging.Add_DictEntry("step", "listadd");
                                objLogging.Add_DictEntry("ontology", relation.objRel.Ontology);
                                objLogging.Add_DictEntry("id_object", relation.objRel.ID_Object);
                                objLogging.Add_DictEntry("id_other", relation.objRel.ID_Other);
                                objLogging.Add_DictEntry("id_relationtype", relation.objRel.ID_RelationType);
                                objLogging.Add_DictEntry("mode", "save");
                                objLogging.Finish_Document();
                            }
                            else
                            {
                                relationsDel.Add(relation.objRel);

                                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                objLogging.Add_DictEntry("step", "listadd");
                                objLogging.Add_DictEntry("ontology", relation.objRel.Ontology);
                                objLogging.Add_DictEntry("id_object", relation.objRel.ID_Object);
                                objLogging.Add_DictEntry("id_other", relation.objRel.ID_Other);
                                objLogging.Add_DictEntry("id_relationtype", relation.objRel.ID_RelationType);
                                objLogging.Add_DictEntry("mode", "delete");
                                objLogging.Finish_Document();
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(relation.objRel.ID_Object) &&
                                !string.IsNullOrEmpty(relation.objRel.ID_Parent_Object) &&
                                !string.IsNullOrEmpty(relation.objRel.ID_Other) &&
                                !string.IsNullOrEmpty(relation.objRel.ID_RelationType) &&
                                relation.objRel.OrderID != null)
                            {
                                relationsSave.Add(relation.objRel);

                                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                objLogging.Add_DictEntry("step", "listadd");
                                objLogging.Add_DictEntry("ontology", relation.objRel.Ontology);
                                objLogging.Add_DictEntry("id_object", relation.objRel.ID_Object);
                                objLogging.Add_DictEntry("id_other", relation.objRel.ID_Other);
                                objLogging.Add_DictEntry("id_relationtype", relation.objRel.ID_RelationType);
                                objLogging.Add_DictEntry("mode", "save");
                                objLogging.Finish_Document();
                            }
                            else
                            {
                                relationsDel.Add(relation.objRel);

                                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                objLogging.Add_DictEntry("step", "listadd");
                                objLogging.Add_DictEntry("ontology", relation.objRel.Ontology);
                                objLogging.Add_DictEntry("id_object", relation.objRel.ID_Object);
                                objLogging.Add_DictEntry("id_other", relation.objRel.ID_Other);
                                objLogging.Add_DictEntry("id_relationtype", relation.objRel.ID_RelationType);
                                objLogging.Add_DictEntry("mode", "delete");
                                objLogging.Finish_Document();
                            }
                        }
                        guid = relation.GUID;
                    }
                }

                if (relationsDel.Any())
                {
                    result = objDBLevel_Work.DelObjectRels(relationsDel);
                    if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                        objLogging.Add_DictEntry("timestamp", DateTime.Now);
                        objLogging.Add_DictEntry("step", "delete relations");
                        objLogging.Add_DictEntry("result", "success");
                        objLogging.Finish_Document();
                    }
                    else
                    {
                        objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                        objLogging.Add_DictEntry("timestamp", DateTime.Now);
                        objLogging.Add_DictEntry("step", "delete relations");
                        objLogging.Add_DictEntry("result", "error");
                        objLogging.Finish_Document();
                    }
                }

            }
            else
            {
                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                objLogging.Add_DictEntry("step", "search multiple relations");
                objLogging.Add_DictEntry("result", "error");
                objLogging.Finish_Document();
            }
        }

        private void Move_Versions()
        {
            

            var objDBLevel_Search1 = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel_Search2 = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel_Search3 = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel_Move = new OntologyModDBConnector(objLocalConfig.Globals);

            var searchClassRels = new List<clsClassRel> { new clsClassRel
            {
                ID_Class_Right = "f30436d62ffc4071af5e3ce708b8c2d9"
            } };

            var objOItem_Result = objDBLevel_Search1.GetDataClassRel(searchClassRels, doIds: false);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var searchVersionRels = new List<clsObjectRel> { new clsObjectRel
                {
                    ID_Parent_Other = "f30436d62ffc4071af5e3ce708b8c2d9"
                } };

                objOItem_Result = objDBLevel_Search2.GetDataObjectRel(searchVersionRels, doIds: false);

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    var found = (from objRel1 in objDBLevel_Search1.ClassRels
                                 join objRel2 in objDBLevel_Search2.ObjectRels on objRel1.ID_Class_Left equals objRel2.ID_Parent_Object
                                 select new clsOntologyItem
                                 {
                                     GUID = objRel2.ID_Other,
                                     Name = objRel2.Name_Other,
                                     GUID_Parent = objRel2.ID_Parent_Other,
                                     Type = objLocalConfig.Globals.Type_Object
                                 }).ToList();

                    var searchVersions = new List<clsOntologyItem> {
                        new clsOntologyItem
                        {
                            GUID_Parent = "f30436d62ffc4071af5e3ce708b8c2d9"
                        } };

                    objOItem_Result = objDBLevel_Search3.GetDataObjects(searchVersions);

                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        var move = (from objVer in objDBLevel_Search3.Objects1
                                    join objVersionOk in found on objVer.GUID equals objVersionOk.GUID into objVersionsOk
                                    from objVersionOk in objVersionsOk.DefaultIfEmpty()
                                    where objVersionOk == null
                                    select new clsOntologyItem
                                    {
                                        GUID = objVer.GUID,
                                        Name = objVer.Name,
                                        GUID_Parent = objVer.GUID_Parent,
                                        Type = objLocalConfig.Globals.Type_Object
                                    }).ToList();

                        //objOItem_Result = objDBLevel_Move.save_Objects(move);
                        objOItem_Result = objTransaction_Objects.move_Objects(move, "32a3d0fa37444021b200245c9b877418");

                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            MessageBox.Show(this, "Die Versionen wurden verschoben.", "Erfolg", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show(this, "Die Versionen konnten nicht verschoben werden!", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }

                    }
                    else
                    {
                        MessageBox.Show(this, "Die Versionen konnten nicht verschoben werden!", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }


                }
                else
                {
                    MessageBox.Show(this, "Die Versionen konnten nicht verschoben werden!", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            


        }

        private void Repair_MP3File()
        {
            var OItem_Class = new clsOntologyItem
            {
                GUID = "0e2285544c7e48e0ba032de51394be4a",
                Name = "mp3-File",
                GUID_Parent = "d84fa125dbce44b091539abeb66ad27f"
            };

            var relationSearch = new List<clsObjectRel> { new clsObjectRel {ID_Parent_Object = OItem_Class.GUID, 
                ID_RelationType = "e07469d9766c443e85266d9c684f944f",
                ID_Parent_Other = "d84fa125dbce44b091539abeb66ad27f" } };

            var objLogging = new clsLogging(objLocalConfig.Globals);

            objLogging.Initialize_Logging(objDataWork_Repair.Log, "mp3tags");

            var objDBLevel_MediaItems = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel_Files = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel_Work = new OntologyModDBConnector(objLocalConfig.Globals);

            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
            objLogging.Add_DictEntry("timestamp", DateTime.Now);
            objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
            objLogging.Add_DictEntry("step", "search mediaitem-relations");
            objLogging.Add_DictEntry("substep", "start");
            objLogging.Finish_Document();

            var result = objDBLevel_MediaItems.GetDataObjectRel(relationSearch, doIds:false);

            

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
                objLogging.Add_DictEntry("step", "search mediaitem-relations");
                objLogging.Add_DictEntry("substep", "end");
                objLogging.Finish_Document();

                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
                objLogging.Add_DictEntry("step", "search files-relations");
                objLogging.Add_DictEntry("substep", "start");
                objLogging.Finish_Document();

                var searchFiles = new List<clsObjectRel> { new clsObjectRel { ID_Parent_Object = "d84fa125dbce44b091539abeb66ad27f",
                    ID_RelationType = "d34d545e9ddf46cebb6f22db1b7bb025",
                    ID_Parent_Other = "6eb4fdd32e254886b288e1bfc2857efb" } };
                    
                result = objDBLevel_Files.GetDataObjectRel(searchFiles,doIds:false);

                if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {

                    objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                    objLogging.Add_DictEntry("timestamp", DateTime.Now);
                    objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
                    objLogging.Add_DictEntry("step", "search files-relations");
                    objLogging.Add_DictEntry("substep", "end");
                    objLogging.Finish_Document();

                    objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                    objLogging.Add_DictEntry("timestamp", DateTime.Now);
                    objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
                    objLogging.Add_DictEntry("step", "save new relations");
                    objLogging.Add_DictEntry("substep", "start");
                    objLogging.Finish_Document();

                    var fileRelations = (from mediaItem in  objDBLevel_MediaItems.ObjectRels
                                        join file in objDBLevel_Files.ObjectRels on mediaItem.ID_Other equals file.ID_Object
                                        select new clsObjectRel { ID_Object = mediaItem.ID_Object,
                                            ID_Parent_Object = mediaItem.ID_Parent_Object,
                                            ID_RelationType = mediaItem.ID_RelationType,
                                            ID_Other = file.ID_Other,
                                            ID_Parent_Other = file.ID_Parent_Other,
                                            OrderID = mediaItem.OrderID,
                                            Ontology = mediaItem.Ontology }).ToList();

                    if (fileRelations.Any())
                    {
                        result = objDBLevel_Work.SaveObjRel(fileRelations);

                        if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                            objLogging.Add_DictEntry("timestamp", DateTime.Now);
                            objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
                            objLogging.Add_DictEntry("step", "save new relations");
                            objLogging.Add_DictEntry("substep", "end");
                            objLogging.Finish_Document();

                            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                            objLogging.Add_DictEntry("timestamp", DateTime.Now);
                            objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
                            objLogging.Add_DictEntry("step", "del old relations");
                            objLogging.Add_DictEntry("substep", "start");
                            objLogging.Finish_Document();

                            if (objDBLevel_MediaItems.ObjectRels.Any())
                            {
                                result = objDBLevel_Work.DelObjectRels(objDBLevel_MediaItems.ObjectRels);
                                if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                {
                                    objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                    objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                    objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
                                    objLogging.Add_DictEntry("step", "del old relations");
                                    objLogging.Add_DictEntry("substep", "end");
                                    objLogging.Finish_Document();
                                }
                                else
                                {
                                    objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                    objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                    objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
                                    objLogging.Add_DictEntry("result", "error");
                                    objLogging.Finish_Document();
                                }

                            }
                            else
                            {
                                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
                                objLogging.Add_DictEntry("step", "save new relations");
                                objLogging.Add_DictEntry("substep", "end");
                                objLogging.Add_DictEntry("result", "no file-relations");
                                objLogging.Finish_Document();
                            }
                        }
                        else
                        {
                            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                            objLogging.Add_DictEntry("timestamp", DateTime.Now);
                            objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
                            objLogging.Add_DictEntry("result", "error");
                            objLogging.Finish_Document();
                        }
                    }
                    else
                    {
                        objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                        objLogging.Add_DictEntry("timestamp", DateTime.Now);
                        objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
                        objLogging.Add_DictEntry("step", "save new relations");
                        objLogging.Add_DictEntry("substep", "end");
                        objLogging.Add_DictEntry("result", "no file-relations");
                        objLogging.Finish_Document();
                    }
                }
                else
                {
                    objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                    objLogging.Add_DictEntry("timestamp", DateTime.Now);
                    objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
                    objLogging.Add_DictEntry("result", "error");
                    objLogging.Finish_Document();
                }

            }
            else
            {
                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                objLogging.Add_DictEntry("mp3_relations", OItem_Class.GUID);
                objLogging.Add_DictEntry("result", "error");
                objLogging.Finish_Document();
            }
        }

        /// <summary>
        ///     Die Klasse enthielt zwei Objekte mit der gleichen ID.
        /// </summary>
        private void Repair_MathematischeKomponenten()
        {
            var OItem_Class = new clsOntologyItem
            {
                GUID = "6ea698d116d74754975673ec3dffb442",
                Name = "Mathematisches Element",
                GUID_Parent = "c2cab57626b34254972addb62067c2be"
            };

            var objectsSearch = new List<clsOntologyItem> { new clsOntologyItem {GUID_Parent = OItem_Class.GUID, 
                Type = objLocalConfig.Globals.Type_Object } };
            var objLogging = new clsLogging(objLocalConfig.Globals);
            objLogging.Initialize_Logging(objDataWork_Repair.Log, "mathekomp");
            

            var objDBLevel = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel_Attribs = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel_Relations_LeftRight = new OntologyModDBConnector(objLocalConfig.Globals);
            var objDBLevel_Relations_RightLeft = new OntologyModDBConnector(objLocalConfig.Globals);

            var delAttribs = false;
            var delLeftRight = false;
            var delRightLeft = false;

            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
            objLogging.Add_DictEntry("timestamp", DateTime.Now);
            objLogging.Add_DictEntry("searchclass_guid", OItem_Class.GUID);
            objLogging.Add_DictEntry("searchclass_name", OItem_Class.Name);
            objLogging.Add_DictEntry("step", "search class");
            objLogging.Add_DictEntry("substep", "start");
            objLogging.Finish_Document();

            var result = objDBLevel.GetDataObjects(objectsSearch);

            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
            objLogging.Add_DictEntry("timestamp", DateTime.Now);
            objLogging.Add_DictEntry("searchclass_guid", OItem_Class.GUID);
            objLogging.Add_DictEntry("searchclass_name", OItem_Class.Name);
            objLogging.Add_DictEntry("count_found", objDBLevel.Objects1.Count());
            objLogging.Add_DictEntry("step", "search class");
            objLogging.Add_DictEntry("substep", "end");
            objLogging.Finish_Document();

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                
                var GuidGroup = (from objObject in objDBLevel.Objects1
                                group objObject by objObject.GUID into objectGroup
                                select new { GUID = objectGroup.Key, Count = objectGroup.Count() }).Where(o => o.Count > 1).ToList();

                var objectItems = (from objObject in objDBLevel.Objects1
                                   join objGuidItem in GuidGroup on objObject.GUID equals objGuidItem.GUID
                                   select objObject).ToList();
                foreach (var guidItem in objectItems)
                {
                    objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                    objLogging.Add_DictEntry("timestamp", DateTime.Now);
                    objLogging.Add_DictEntry("guid_item", guidItem.GUID);
                    objLogging.Add_DictEntry("name_item", guidItem.Name);
                    objLogging.Add_DictEntry("guid_class", guidItem.GUID_Parent);
                    objLogging.Add_DictEntry("step", "determine multiple guids");
                    objLogging.Add_DictEntry("substep", "end");
                    objLogging.Finish_Document();    
                }


                if (GuidGroup.Any())
                {
                    var attSearch = GuidGroup.Select(gg => new clsObjectAtt { ID_Object = gg.GUID }).ToList();

                    objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                    objLogging.Add_DictEntry("timestamp", DateTime.Now);
                    objLogging.Add_DictEntry("step", "determine related attributes");
                    objLogging.Add_DictEntry("substep", "start");
                    objLogging.Finish_Document();

                    result = objDBLevel_Attribs.GetDataObjectAtt(attSearch, doIds: false);

                    if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        foreach (var oAtt in objDBLevel_Attribs.ObjAtts)
                        {
                            delAttribs = true;
                            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                            objLogging.Add_DictEntry("timestamp", DateTime.Now);
                            objLogging.Add_DictEntry("object", oAtt.ID_Attribute + ";" + oAtt.ID_AttributeType + ";" + oAtt.ID_Object + ";" + oAtt.ID_DataType + ";" + oAtt.Val_Named);
                            objLogging.Add_DictEntry("objecttype", "object-attribute");
                            objLogging.Add_DictEntry("step", "determine related attributes");
                            objLogging.Add_DictEntry("substep", "log");
                            objLogging.Finish_Document();
                        }
                        objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                        objLogging.Add_DictEntry("timestamp", DateTime.Now);
                        objLogging.Add_DictEntry("step", "determine related attributes");
                        objLogging.Add_DictEntry("substep", "end");
                        objLogging.Finish_Document();


                        var relLRSearch = GuidGroup.Select(gg => new clsObjectRel { ID_Object = gg.GUID }).ToList();

                        objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                        objLogging.Add_DictEntry("timestamp", DateTime.Now);
                        objLogging.Add_DictEntry("step", "determine leftsided-relations");
                        objLogging.Add_DictEntry("substep", "start");
                        objLogging.Finish_Document();

                        result = objDBLevel_Relations_LeftRight.GetDataObjectRel(relLRSearch, doIds: false);

                        if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            foreach (var oRel in objDBLevel_Relations_LeftRight.ObjectRels)
                            {
                                delLeftRight = true;
                                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                objLogging.Add_DictEntry("object", oRel.ID_Object + ";" + oRel.ID_Other + ";" + oRel.ID_RelationType + ";" + oRel.OrderID.ToString());
                                objLogging.Add_DictEntry("objecttype", "object-relation-lr");
                                objLogging.Add_DictEntry("step", "determine leftsided-relations");
                                objLogging.Add_DictEntry("substep", "log");
                                objLogging.Finish_Document();
                            }

                            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                            objLogging.Add_DictEntry("timestamp", DateTime.Now);
                            objLogging.Add_DictEntry("step", "determine leftsided-relations");
                            objLogging.Add_DictEntry("substep", "end");
                            objLogging.Finish_Document();

                            var relRLSearch = GuidGroup.Select(gg => new clsObjectRel { ID_Other = gg.GUID }).ToList();

                            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                            objLogging.Add_DictEntry("timestamp", DateTime.Now);
                            objLogging.Add_DictEntry("step", "determine rightside-relations");
                            objLogging.Add_DictEntry("substep", "start");
                            objLogging.Finish_Document();

                            result = objDBLevel_Relations_RightLeft.GetDataObjectRel(relRLSearch, doIds: false);

                            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                            {
                                foreach (var oRel in objDBLevel_Relations_RightLeft.ObjectRels)
                                {
                                    delRightLeft = true;
                                    objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                    objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                    objLogging.Add_DictEntry("object", oRel.ID_Object + ";" + oRel.ID_Other + ";" + oRel.ID_RelationType + ";" + oRel.OrderID.ToString());
                                    objLogging.Add_DictEntry("objecttype", "object-relation-rl");
                                    objLogging.Add_DictEntry("step", "determine rightside-relations");
                                    objLogging.Add_DictEntry("substep", "log");
                                    objLogging.Finish_Document();
                                }

                                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                objLogging.Add_DictEntry("step", "determine rightside-relations");
                                objLogging.Add_DictEntry("substep", "end");
                                objLogging.Finish_Document();

                                var objDBLevel_Del = new OntologyModDBConnector(objLocalConfig.Globals);

                                if (delAttribs)
                                {
                                    var attributeDeletes = GuidGroup.Select(gg => new clsObjectAtt { ID_Object = gg.GUID }).ToList();

                                    objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                    objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                    objLogging.Add_DictEntry("step", "delete attributes");
                                    objLogging.Add_DictEntry("substep", "start");
                                    objLogging.Finish_Document();

                                    result = objDBLevel_Del.DelObjectAtts(attributeDeletes);

                                    objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                    objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                    objLogging.Add_DictEntry("step", "delete attributes");
                                    objLogging.Add_DictEntry("substep", "end");
                                    objLogging.Finish_Document();
                                }

                                if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                {
                                    if (delLeftRight)
                                    {
                                        var leftRightDeletes = GuidGroup.Select(gg => new clsObjectRel { ID_Object = gg.GUID }).ToList();

                                        objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                        objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                        objLogging.Add_DictEntry("step", "delete left-right");
                                        objLogging.Add_DictEntry("substep", "start");
                                        objLogging.Finish_Document();

                                        result = objDBLevel_Del.DelObjectRels(leftRightDeletes);

                                        objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                        objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                        objLogging.Add_DictEntry("step", "delete left-right");
                                        objLogging.Add_DictEntry("substep", "end");
                                        objLogging.Finish_Document();
                                    }

                                    if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                    {
                                        if (delRightLeft)
                                        {
                                            var rightLeftDeletes = GuidGroup.Select(gg => new clsObjectRel { ID_Other = gg.GUID }).ToList();

                                            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                            objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                            objLogging.Add_DictEntry("step", "delete right-left");
                                            objLogging.Add_DictEntry("substep", "start");
                                            objLogging.Finish_Document();

                                            result = objDBLevel_Del.DelObjectRels(rightLeftDeletes);

                                            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                            objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                            objLogging.Add_DictEntry("step", "delete right-left");
                                            objLogging.Add_DictEntry("substep", "end");
                                            objLogging.Finish_Document();
                                        }

                                        if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                        {
                                            var objectDeletes = GuidGroup.Select(gg => new clsOntologyItem { GUID = gg.GUID }).ToList();

                                            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                            objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                            objLogging.Add_DictEntry("step", "delete object");
                                            objLogging.Add_DictEntry("substep", "start");
                                            objLogging.Finish_Document();

                                            result = objDBLevel_Del.DelObjects(objectDeletes);

                                            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                            objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                            objLogging.Add_DictEntry("step", "delete object");
                                            objLogging.Add_DictEntry("substep", "end");
                                            objLogging.Finish_Document();
                                        }
                                        else
                                        {
                                            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                            objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                            objLogging.Add_DictEntry("result", "error");
                                            objLogging.Finish_Document();
                                        }
                                    }
                                    else
                                    {
                                        objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                        objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                        objLogging.Add_DictEntry("result", "error");
                                        objLogging.Finish_Document();
                                    }
                                }
                                else
                                {
                                    objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                    objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                    objLogging.Add_DictEntry("result", "error");
                                    objLogging.Finish_Document();
                                }
                            }
                            else
                            {
                                objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                                objLogging.Add_DictEntry("timestamp", DateTime.Now);
                                objLogging.Add_DictEntry("result", "error");
                                objLogging.Finish_Document();
                            }



                        }
                        else
                        {
                            objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                            objLogging.Add_DictEntry("timestamp", DateTime.Now);
                            objLogging.Add_DictEntry("result", "error");
                            objLogging.Finish_Document();
                        }

                    }
                    else
                    {
                        objLogging.Init_Document(objLocalConfig.Globals.NewGUID);
                        objLogging.Add_DictEntry("timestamp", DateTime.Now);
                        objLogging.Add_DictEntry("result", "error");
                        objLogging.Finish_Document();
                    }

                    

                    

                    
                }
                objLogging.Flush_Documents();

                

                
            }


        }
    }
}
